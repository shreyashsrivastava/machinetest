from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
import json
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def calculate(request):
    if request.user.is_authenticated:
        if request.method == 'GET':
            return HttpResponse("GET REQUEST")
        sum=0
        x = int(request.POST['x'])
        n = int(request.POST['n'])
        if x != 0 and n>1:
            for i in range(1,  n+1):
                sum+=(1/(x**i))
        else:
            return "'x' should not be equal to 0 and 'n' should be greater than 1"
        response_data = {
            'Result': sum,
        }
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    else:
        return JsonResponse({'message':'Login Required'})
