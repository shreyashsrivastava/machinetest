from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.views.decorators import csrf
from django.views.decorators.csrf import csrf_exempt
from user.models import User

# Create your views here.
@csrf_exempt
def login_view(request):
    if request.user.is_authenticated:
        return JsonResponse({"message":"User already logged in."})
    email = request.POST['email']
    password = request.POST['password']
    user = authenticate(request, email=email, password=password)
    if user is not None:
        login(request, user)
        return JsonResponse({"message":"User logged in successfully."})
    return JsonResponse({"message":"Email or password incorrect."})


def logout_view(request):
    if request.user.is_authenticated:
        logout(request)
        return JsonResponse({"message":"Logout Successfully"})
    return JsonResponse({"message":"Already Logout"})

@csrf_exempt
def register_view(request):
    if request.user.is_authenticated:
        return JsonResponse({"message":"You are already logged in."})
    
    email = request.POST['email']
    password1 = request.POST['password1']
    password2 = request.POST['password2']
    phone = request.POST['phone']

    if password1 != password2:
        return JsonResponse({"message":"You are already logged in."})

    try:
        user, created = User.objects.get_or_create(email=email, phone_number=phone)
        if created:
            user.set_password(password1)
            user.save()
            return JsonResponse({"message":"User created successfully"})
        else:
            return JsonResponse({"message":f"User with emai '{user.email}' already exist."})
    
    except Exception as e:
        return JsonResponse({"message":f"{e}"})







    