from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout

# Create your views here.
def login_view(request):
    if request.method == 'GET':
        return render(request, 'user/login.html')

    email = request.POST['email']
    password = request.POST['password']
    user = authenticate(request, email=email, password=password)
    if user is not None:
        login(request, user)
    return render(request, 'user/login.html', context={'message':"user logged in successfully"})


def logout_view(request):
    logout(request)