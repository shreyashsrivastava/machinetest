def calcExpr(p, q, e, opr):
    if opr=='+':
        return (p+(1/q))**e
    if opr=='-':
        return (p-(1/q))**e

def calcFullExpr(x, y, a, b):
    m = calcExpr(x, y, a, '+') * calcExpr(x, y, b, '-')
    n = calcExpr(y, x, a, '+') * calcExpr(y, x, b, '-')
    return (m/n)

print(calcFullExpr(10, 5, 2, 2))