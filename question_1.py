def calc(x, n):
    sum=0
    if x != 0 and n>1:
        for i in range(1,  n+1):
            sum+=(1/(x**i))
    else:
        return "'x' should not be equal to 0 and 'n' should be greater than 1"
    
    return sum

def calcRecur(x, n):
    if n==1:
        return 1/x
    if x!=0 and n>1:
        ans = calcRecur(x, n-1)
    else:
        return "'x' should not be equal to 0 and 'n' should be greater than 1"
    return ans+1/(x**n)

print("Result: ", calc(10, 10))
print("Result using recursion: ", calcRecur(10, 10))
