## For running project

For running the project first create a virtual envirnoment and activate it.
Then go to `question_4_5/src` and install dependencies by running:

```
pip install -r requirements.txt
```

Then, for starting a local development server run:

```
python manage.py runserver
```

## API Endpoints

Register User: `http://127.0.0.1:9000/api/user/register/`

Login: `POST http://127.0.0.1:9000/api/user/login/`

Logout: `GET http://127.0.0.1:9000/api/user/logout`

Calcluate: `POST http://127.0.0.1:9000/api/v1/calculate/`